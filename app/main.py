from fastapi import FastAPI
from fastapi.responses import RedirectResponse
from app.routers import standard, protected

app = FastAPI(title="CSIT NDP API", description="## Onward As One")

# Include any other app configurations or route definitions here
app.include_router(standard.router, tags=["General APIs"])
app.include_router(protected.router, tags=["Protected APIs"])


@app.get("/", include_in_schema=False)
def redirect_to_docs():
    # Redirect the root URL ("/") to the API documentation
    return RedirectResponse("/docs")
