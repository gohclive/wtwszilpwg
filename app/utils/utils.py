from math import sqrt


def calculate_distance(lat1, long1, lat2, long2):
    """
    Calculates the Euclidean distance between two sets of coordinates (latitude and longitude).

    Parameters:
    lat1, long1, lat2, long2 (float): Latitude and longitude of two points in degrees.

    Returns:
    distance (float): The Euclidean distance between the two points.
    """
    dlat = lat2 - lat1
    dlong = long2 - long1
    return sqrt(dlat**2 + dlong**2)
